## Frontend Exercise Solution

The solution I came up with work with either div, dl or any other tag, the important thing is to keep the structure.

Solution I developed includes:
* custom JS ES6 no frameworks
* sass
* webpack
* babel
* some [SUIT](https://suitcss.github.io/) naming convensions
* ability to use what ever dom element and not only DT & DD, the code automates the building of the Accordian.


## karma tests run:
karm start

## they use for testing:
- cypress.io
- visual studio code

look at jest:
- jest

## Trouble shooting
issues with installing karma use:
- node node_modules/karma/bin/karma init karma.conf.js

