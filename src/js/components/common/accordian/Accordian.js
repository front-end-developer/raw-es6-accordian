/**
 * Created by Mark Webley on 07/07/2018.
 */

export default class Accordian {
    constructor(className, group = 'dl', service = null){
        this.el = document.querySelector(`.${className}`);
        this.children = this.el.querySelector(group).children;
        this.service = service;
        this.init();
    }

    closeAllContainers(domElement){
        Array.prototype.forEach.call(this.children, (item, index) => {
            if (item !== domElement && item.classList.contains('accordian-btn')) {
                item.nextElementSibling.classList.remove('active');
            }
            if (item !== domElement && item.classList.contains('accordian-container')) {
                item.classList.add('hidden');
            }
        });
    }

    closeContainer(container) {
        container.classList.add('hidden');
    }

    toggleAccordian(domElement) {
        this.closeAllContainers(domElement);
        const nodeSibling = domElement.nextElementSibling;
        if (domElement.classList.contains('active')) {
            domElement.classList.remove('active');
            nodeSibling.classList.add('hidden');
        } else {
            domElement.classList.add('active');
            nodeSibling.classList.remove('hidden');
        }
    }

    init() {
        Array.prototype.forEach.call(this.children, (item, index) => {
            if ((index+1) % 2 === 0) {
                item.classList.add('accordian-container', 'animated', 'fadeInUp', 'hidden');
            } else {
                item.classList.add('accordian-btn');
                item.onclick = (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    this.toggleAccordian(item);
                };
            }
        });
        this.el.style.setProperty('display', 'block');
    }

    getService() {
        if (this.service !== null) {
            console.log('If any data get subscribe to the service and populate the accordian dynamically');
            // this.service.getData().subscribe({});
        }
    }

}