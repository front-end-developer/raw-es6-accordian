/**
 * Created by Mark Webley on 08/07/2018.
 */
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
export default class MarkWebleyService {
    constructor(){
        this.API = {
           getIpsomLorem = '/webservices/jsontest/pages.json';
        }
    }

    getData() {
        let observable = Observable.fromPromise(fetch(this.API.getIpsomLorem).then(response => response.json()));
        return observable;
    }
}