/**
 * Created by Mark Webley on 08/07/2018.
 */
{
    "services": [
        {
            "id": "1",
            "title": "Business Application Development",
            "description": "Call Mark Webley, for application development of your apps for desktop and mobile devices"
        }, {
            "id": "2",
            "title": "IPAD & Desktop Apps",
            "description": "Call Mark Webley, for application development of your apps for desktop and mobile devices"
        }
    ]
}