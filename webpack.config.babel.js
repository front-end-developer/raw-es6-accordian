/**
 * Created by Mark Webley on 08/07/2018.
 */
import {resolve} from 'path';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackUglifyJsPlugin from 'webpack-uglify-js-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import 'babel-polyfill';

const webPackSettings = (env) => {
    const webpackConfig = {
        context: __dirname,
        entry: {
            // vendors: ["font-awesome-sass-loader!./font-awesome-sass.config.js"],
            app: [
                'babel-polyfill',
                './src/js/app.js'
            ]
        },
        target: 'web',
        output: {
            crossOriginLoading: "anonymous",
            path: resolve(__dirname + '/build'),
            filename: 'bundle.[name].min.js',
            sourceMapFilename: '[name].map',
            publicPath: ''
        },
        devServer: {
            publicPath: resolve('/build/'),
            historyApiFallback: true
        },
        devtool: 'source-map', // 'cheap-module-source-map',
        resolve: {
            extensions: ['.js', '.jsx', '.ejs', '.json', '.html'],
        },

        stats: {
            colors: true,
            reasons: true,
            chunks: true
        },

        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.js$/,
                    loaders: ['babel-loader', 'eslint-loader'],
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /\.ejs/,
                    use: [{
                        loader: 'ejs-loader'
                    }],
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /\.json$/,
                    use: [{
                        loader: 'json-loader'
                    }],
                    exclude: [/node_modules/, /lib/]
                }, {
                    test: /\.js$/,
                    loaders: ['babel-loader'],
                    exclude: [/node_modules/, /lib/]
                }, {
                    test: /\.jsx$/,
                    enforce: "pre",
                    loaders: ['babel-loader'],
                    exclude: [/node_modules/, /lib/]
                }, {
                    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 10000,
                                mimetype: 'application/font-woff'
                            }
                        }
                    ]
                }, {
                    // test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*$|$)/,
                    use: [
                        { loader: 'file-loader' }
                    ]
                }, {
                    test: /\.scss$/,
                    use: [{
                        loader: "style-loader"
                    }, {
                        loader: "css-loader",
                        options: {
                            url: false,
                            minimize: true,
                            sourceMap: true
                        }
                    }, {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true,
                            includePaths: ["./src/assets/scss/style.scss"]
                        }
                    }]
                },
                {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    url: false
                                }
                            },
                            {
                                loader: 'autoprefixer-loader'
                            }
                        ]
                    })
                    //loaders: ['style-loader', 'css-loader', 'autoprefixer-loader']
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin('styles.[name].min.css'),

            new WebpackUglifyJsPlugin({
                cacheFolder: resolve(__dirname, 'build/'),
                debug: true,
                minimize: true,
                sourceMap: true,
                output: {
                    comments: true
                },
                compressor: {
                    warnings: true,
                    keep_fnames: true
                },
                mangle: {
                    keep_fnames: true
                }
            })
        ]
    }

    console.log('NODE ENVIRONMENT: ', process.env.NODE_ENV);
    return webpackConfig;
}

module.exports = webPackSettings;